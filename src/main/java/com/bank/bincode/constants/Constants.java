package com.bank.bincode.constants;


/******************************************************************************************************************
*
* 常量值
* 
* @author junrong
* @date 2016年6月4日 下午4:46:02
* @version 1.0
*****************************************************************************************************************/
public class Constants {

	/**
	 * 返回json相关
	 */
	public static final String RESULTCODE = "resultCode";
	/**
	 * 具体错误代码
	 */
	public static final String ERRORCODE = "errorCode";

	public static final String RESULTMSG = "resultMsg";

	public static final String RESULTDATA = "resultData";

	public static final String RESULT = "result";
	
	public static final String RESULT_ALL_ROWS = "allRows";

	public static final String RESULT_LIST = "list";

	public static final int DEFAULTORDER = 0;

	public static final int PROFITAMOUNTORDER = 1;

	public static final int PERIODORDER = 2;

	public static final int LEFTPROGRESSORDER = 3;
 

	public static class ResultCode {

		public static final int UNLOGIN = -1;
		public static final int SUCCESS = 0;
		public static final int PARAMERROR = 1;
		public static final int SERVERERROR = 2;
		public static final int TOKENERROE = 3;
		public static final int FAILURE = 4;
	}

	public static enum ImageType {
		png("png");

		private String name;

		private ImageType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}
	
}

