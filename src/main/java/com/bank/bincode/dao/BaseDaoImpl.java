package com.bank.bincode.dao;

import javax.annotation.Resource;


import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport; 

import com.ibatis.sqlmap.client.SqlMapClient;


public class BaseDaoImpl  extends SqlMapClientDaoSupport  {

    @Resource 
    public void init(SqlMapClient sqlMapClient) {
        this.setSqlMapClient(sqlMapClient);
    }
}
